package com.nucpee.services;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.SignedObject;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.stereotype.Service;

import com.nucpee.modal.Currency;
import com.nucpee.modal.Customer;
import com.nucpee.modal.Device;
import com.nucpee.modal.Note;
@Service
public class ObjectSigningExample {

	public static void main(String[] args) throws IllegalBlockSizeException, IOException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, SignatureException {

		
		
		//check Sign
		Note note = new Note(150);
		KeyPair keyPair = null;
		try {
			keyPair = generateKeyPair();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SignedObject s = SignObject(note,keyPair);
		
		System.out.println(VerifySign(keyPair.getPublic(), s));

	}
	public static SignedObject SignDevice(Customer cust,KeyPair keyPair) {
		try {

			PrivateKey privateKey = keyPair.getPrivate();
			PublicKey publicKey = keyPair.getPublic();

			// We can sign Serializable objects only

			Device device = new Device(cust, publicKey.toString());
			

			String unsignedObject = SerializationUtil.serialize(device);

			Signature signature = Signature.getInstance(privateKey
					.getAlgorithm());
			SignedObject signedObject = new SignedObject(unsignedObject,
					privateKey, signature);

			return signedObject;

		} catch (SignatureException e) {
		} catch (InvalidKeyException e) {
		} catch (NoSuchAlgorithmException e) {
		} catch (IOException e) {
		}
		return null;
	}
	
	public static SignedObject SignObject(Note note,KeyPair keyPair) {
		try {

			PrivateKey privateKey = keyPair.getPrivate();
			PublicKey publicKey = keyPair.getPublic();

			// We can sign Serializable objects only

			Currency currency = new Currency(note, publicKey.toString());

			String unsignedObject = SerializationUtil.serialize(currency);

			Signature signature = Signature.getInstance(privateKey
					.getAlgorithm());
			SignedObject signedObject = new SignedObject(unsignedObject,
					privateKey, signature);

			return signedObject;

		} catch (SignatureException e) {
		} catch (InvalidKeyException e) {
		} catch (NoSuchAlgorithmException e) {
		} catch (IOException e) {
		}
		return null;
	}


	public static KeyPair generateKeyPair() throws NoSuchAlgorithmException {
		// Generate a 1024-bit Digital Signature Algorithm (DSA) key pair
		KeyPairGenerator keyPairGenerator = KeyPairGenerator
				.getInstance("DSA");
		keyPairGenerator.initialize(1024);
		KeyPair keyPair = keyPairGenerator.genKeyPair();
		return keyPair;
	}

	public static boolean VerifySign(PublicKey publicKey,
			SignedObject signedObject) throws SignatureException {
		String unsignedObject;
		// Verify the signed object
		Signature sig = null;
		try {
			sig = Signature.getInstance(publicKey.getAlgorithm());
		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		boolean verified = false;
		try {
			verified = signedObject.verify(publicKey, sig);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return verified;

		/*
		 * System.out.println("Is signed Object verified ? " + verified);
		 * 
		 * // Retrieve the object unsignedObject = (String)
		 * signedObject.getObject();
		 * 
		 * System.out.println("Unsigned Object : " +
		 * SerializationUtil.deserialize(unsignedObject));
		 */
	}

}