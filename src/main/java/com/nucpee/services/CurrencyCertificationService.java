package com.nucpee.services;

import java.io.IOException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.SignedObject;

import org.apache.taglibs.standard.lang.jstl.test.beans.PublicBean1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nucpee.Certificates.DeviceCertificate;
import com.nucpee.modal.Customer;
import com.nucpee.modal.Device;

@Service
public class CurrencyCertificationService {
	@Autowired
	private ObjectSigningExample signService;
	@Autowired
	private noteService noteSer;
	
	@Autowired
	private SerializationUtil serializationUtil;

	public KeyPair kp;

	public void generateKeyPairForCurency() {
		try {
			kp = signService.generateKeyPair();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			System.out.println("Device Registration Exception");
		}
	}

	public String signDevice(Customer customer) throws IOException {

		SignedObject signObj = signService.SignDevice(customer, kp);
		DeviceCertificate dc = new DeviceCertificate(signObj,
				serializationUtil.serialize(kp.getPublic()));
		try {
			return serializationUtil.serialize(dc);
		} catch (IOException e) {

			e.printStackTrace();
			System.out.println("Device Registration Exception");
		}
		return null;
	}

	public boolean VerifyDevice(Customer customer)
			throws ClassNotFoundException, IOException {
		DeviceCertificate s = (DeviceCertificate) serializationUtil
				.deserialize(customer.getDeviceCertificate());
		PublicKey pk = s.getPk();
		try {
			return signService.VerifySign(pk, s.getS());
		} catch (SignatureException e) {
			e.printStackTrace();
			System.out.println("Device Verification Exception");
			return false;
		}

	}
}
