package com.nucpee.services;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.springframework.stereotype.Service;

/**
 * @author bchild
 * 
 */
@Service
public class SerializationUtil {
	// deserialize to Object from given file

	public static Object deserialize(String serializedObject)
			throws IOException, ClassNotFoundException {
		// deserialize the object
		Object obj=null;
		try {
			byte b[] = serializedObject.getBytes();
			ByteArrayInputStream bi = new ByteArrayInputStream(b);
			ObjectInputStream si = new ObjectInputStream(bi);
			obj = (Object) si.readObject();
		} catch (Exception e) {
			System.out.println(e);
		}

		return obj;
	}

	// serialize the given object and save it to file
	public static String serialize(Object obj)
			throws IOException {
		// serialize the object
		String serializedObject=null;
		try {
			ByteArrayOutputStream bo = new ByteArrayOutputStream();
			ObjectOutputStream so = new ObjectOutputStream(bo);
			so.writeObject(obj);
			so.flush();
			 serializedObject = bo.toString();
		} catch (Exception e) {
			System.out.println(e);
		}
		return serializedObject;
	}
}
