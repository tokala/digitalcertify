package com.nucpee.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.nucpee.modal.Customer;
import com.nucpee.modal.Note;
import com.nucpee.modal.Person;
import com.nucpee.services.CustomerService;
import com.nucpee.services.ObjectSigningExample;
import com.nucpee.services.PersonService;
import com.nucpee.services.noteService;
 
@Controller
@RequestMapping("/customer")
public class HomeController {
 
  @Autowired private PersonService personSvc;
 
  @Autowired private ObjectSigningExample signService;
  @Autowired private CustomerService customerService;
  

  
  /**
   * Requests to http://localhost:8080/hello will be mapped here.
   * Everytime invoked, we pass list of all persons to view
   */
  @RequestMapping(method = RequestMethod.GET)
  public String listAll(Model model) {
    model.addAttribute("persons", personSvc.getAll());
    return "home";
  }
   
  /**
   * POST requests to http://localhost:8080/hello/addPerson goes here.
   * The new person data is passed from HTML from and bound into the
   * Person object.
   */
  @RequestMapping(value = "/register", method = RequestMethod.POST)
  public String addPerson(@ModelAttribute Customer customer) {
	  
	  customerService.save(customer);
	  
    
    return "redirect:/";
  }
}