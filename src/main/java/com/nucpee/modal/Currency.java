package com.nucpee.modal;

import java.io.Serializable;

public class Currency implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1187371047903480017L;
	private Note note;
	private String pubKey;

	public Note getNote() {
		return note;
	}

	public void setNote(Note note) {
		this.note = note;
	}

	public String getPubKey() {
		return pubKey;
	}

	public void setPubKey(String pubKey) {
		this.pubKey = pubKey;
	}

	public Currency(Note note, String pubKey) {
		super();
		this.note = note;
		this.pubKey = pubKey;
	}
	@Override
	public String toString() {
		return note.toString()+"\n publickey:"+pubKey;
	}

}