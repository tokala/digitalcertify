package com.nucpee.modal;

public class Device {
	private Customer customer;
	private String pubKey;
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public String getPubKey() {
		return pubKey;
	}
	public void setPubKey(String pubKey) {
		this.pubKey = pubKey;
	}
	public Device(Customer customer, String pubKey) {
		super();
		this.customer = customer;
		this.pubKey = pubKey;
	}
	
}
