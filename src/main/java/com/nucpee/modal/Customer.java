package com.nucpee.modal;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.UniqueConstraint;

@Entity
public class Customer implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private int cutID;

	public int getCutID() {
		return cutID;
	}

	public void setCutID(int cutID) {
		this.cutID = cutID;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	private String IMEI;
	private String bankAccountNum;
	private String deviceCertificate;

	public String getIMEI() {
		return IMEI;
	}

	public void setIMEI(String iMEI) {
		IMEI = iMEI;
	}

	public Customer(String iMEI, String bankAccountNum, String deviceCertificate) {
		super();
		IMEI = iMEI;
		this.bankAccountNum = bankAccountNum;
		this.deviceCertificate = deviceCertificate;
	}

	public String getBankAccountNum() {
		return bankAccountNum;
	}

	public void setBankAccountNum(String bankAccountNum) {
		this.bankAccountNum = bankAccountNum;
	}

	public String getDeviceCertificate() {
		return deviceCertificate;
	}

	public void setDeviceCertificate(String deviceCertificate) {
		this.deviceCertificate = deviceCertificate;
	}

}
