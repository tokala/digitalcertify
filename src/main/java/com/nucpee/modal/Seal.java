package com.nucpee.modal;

import java.security.Security;

import javax.crypto.Cipher;
import javax.crypto.SealedObject;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class Seal {
	 static String algorithm = "PBEWithMD5AndDES";
	    static char[] passPherase = "secretpass".toCharArray();
	    static byte[] salt = "a9v5n38s".getBytes();
	    static Note note=new Note(123);
	  
	  
	    
	    public static  SealedObject encrypt(String data) throws Exception{
	        PBEParameterSpec pbeParamSpec = new PBEParameterSpec(salt,20);
	        PBEKeySpec pbeKeySpec = new PBEKeySpec(passPherase);
	        SecretKeyFactory secretKeyFactory = 
	            SecretKeyFactory.getInstance(algorithm);
	        SecretKey secretKey = secretKeyFactory.generateSecret(pbeKeySpec);

	        Cipher cipher = Cipher.getInstance(algorithm);
	        cipher.init(Cipher.ENCRYPT_MODE,secretKey,pbeParamSpec);

	        return new SealedObject(data,cipher);
	    }
	    public static String decrypt(SealedObject sealedObject) throws Exception{
	        PBEParameterSpec pbeParamSpec = new PBEParameterSpec(salt,20);
	        PBEKeySpec pbeKeySpec = new PBEKeySpec(passPherase);
	        SecretKeyFactory secretKeyFactory = 
	            SecretKeyFactory.getInstance(algorithm);
	        SecretKey secretKey = secretKeyFactory.generateSecret(pbeKeySpec);

	        Cipher cipher = Cipher.getInstance(algorithm);
	        cipher.init(Cipher.DECRYPT_MODE,secretKey,pbeParamSpec);
	        return (String)sealedObject.getObject(cipher);
	    }
	  
	   /* public static void main(String[] args) {
	        try{
	            Security.addProvider(new BouncyCastleProvider());
	            String secretData = SerializationUtil.serialize(note);
	            SealedObject encryptedString = encrypt(secretData);
	            System.out.println(encryptedString);
	            String decryptedString = decrypt(encryptedString);
	            Note c=(Note)SerializationUtil.deserialize(decryptedString);
	            
	            System.out.println(c.getNoteValue());
	        }catch( Exception e ) { 
	            System.out.println(e.toString());
	        }
	    }*/
	    
	}


