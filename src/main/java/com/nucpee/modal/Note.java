package com.nucpee.modal;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity
public class Note implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -217345133818100039L;
	private int NoteValue=1;
	@Id
	@GeneratedValue
	private int noteNumber;
	public int getNoteValue() {
		return NoteValue;
	}
	public void setNoteValue(int noteValue) {
		NoteValue = noteValue;
	}
	public int getNoteNumber() {
		return noteNumber;
	}
	public void setNoteNumber(int noteNumber) {
		this.noteNumber = noteNumber;
	}
	public Note(int noteNumber) {
		super();
		this.noteNumber = noteNumber;
	}
	public Note() {
		super();
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
	
		return String.valueOf(this.getNoteNumber());
	}
	
	
}