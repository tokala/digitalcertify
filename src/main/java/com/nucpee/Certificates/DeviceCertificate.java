package com.nucpee.Certificates;

import java.io.IOException;
import java.security.PublicKey;
import java.security.SignedObject;

import com.nucpee.services.SerializationUtil;

public class DeviceCertificate {

	private SignedObject s;
	private PublicKey pk;
	public DeviceCertificate(SignedObject signObj, String serialize) {
		this.s=signObj;
		try {
			this.pk=(PublicKey) SerializationUtil.deserialize(serialize);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Device Registration Exception");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Device Registration Exception");
		}
	}
	public SignedObject getS() {
		return s;
	}
	public void setS(SignedObject s) {
		this.s = s;
	}
	public PublicKey getPk() {
		return pk;
	}
	public void setPk(PublicKey pk) {
		this.pk = pk;
	}
	
}
