/*package com.nupee.web;

import java.util.Collection;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;

import com.nupee.model.Customer;
import com.nupee.model.Currency;
public interface NupeeBankAPI {

	// 1. device certificate
	// 2. updateAccount

	public static final String NBA_CUST = "/customer";

	public static final String NBA_CUST_REGISTER = NBA_CUST + "/register";
	public static final String NBA_CUST_UPDATE_ACCOUNT = NBA_CUST + "/updateBalance";

	*//**
	 * This endpoint in the API returns a list of the currency that have
	 * been added to the server. The Currency objects should be returned as
	 * JSON. 
	 * 
	 * To manually test this endpoint, run your server and open this URL in a browser:
	 * http://localhost:8080/customer/updateBalance.
	 * 
	 * @return
	 *//*
	@GET(NBA_CUST_UPDATE_ACCOUNT)
	public Collection<Currency> updateAccountBalance();
	
	*//**
	 * This endpoint allows clients to register a customer by sending POST requests
	 * that have an application/json body containing the Customer object information. 
	 * 
	 * To manually test this endpoint, run your server and open this URL in a browser:
	 * http://localhost:8080/customer/register.
	 * @return
	 *//*
	@POST(NBA_CUST_UPDATE_ACCOUNT)
	public Customer registerCustomer(@Body Customer c);
}
*/